# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 14:48:27 2019

@author: MARONE Cheikh Ahmet Tidiane Chérif
"""
import numpy as np
class Perceptron(object):

	"""
	le nombre d’inputs du perceptron, 
	le nombre d’epochs (càd le nombre de boucle d’apprentissage) 
	et un learning rate : le taux d'apprentissage détermine la vitesse à laquelle les poids (dans le cas d'un réseau neuronal) ou les cooefficents (en cas de régression linéaire ou de régression logistique) changent.
	"""
	
	def __init__(self, nombreInput, nombreEpochs, learningRate): 
		#super(Character, self).__init__()
		self.nombreInput = nombreInput 
		self.nombreEpochs = nombreEpochs
		self.learningRate = learningRate
		self.poids = np.array([0,0,0]) 
		self.biais = 1

	"""Les mutateurs"""

	def _get_nombreInput(self):
		return self.nombreInput

	def _get_nombreEpochs(self):
		return self.nombreEpochs

	def _get_learningRate(self):
		return self.learningRate

	"""Les Accesseurs"""
	
	def _set_nombreInput(self, nombreInput):
		self.nombreInput = nombreInput

	def _set_nombreEpochs(self, nombreEpochs):
		self.nombreEpochs = nombreEpochs
	
	def _set_learningRate(self, learningRate):
		self.learningRate = learningRate

	def _get_poids(self):
		for i in np.arange(3):
			print(self.poids[i])

	def predict(self, paireInput):
		x = self.poids[0] + (paireInput[0] * self.poids[1]) + (paireInput[1] * self.poids[2])
		if x <=0 :
			x = 0
		else:
			x = 1
		return x

	def train(self, lesInputs,lesSortie):
		j=0
		poidsCorrige = 0
		#EntreePoids = self.biais
        #fonction relue 6
		print('le poidsCorrige' + str(poidsCorrige))
		valeur = True
		print('la valeur' + str(valeur))	
		for i in np.arange(self.nombreEpochs):
			
			for paireInput in lesInputs:
				while valeur:
					sortieAttendu = lesSortie[j]
					sortieObserve = self.predict(paireInput)
					print('la input 1 = ' + str(paireInput[0]))
					print('la input 2 = ' + str(paireInput[1]))
					print('la sortieObserve' + str(sortieObserve))
					print('la sortieAttendu' + str(sortieAttendu))
					valeur = (sortieObserve != sortieAttendu)		
					print('la valeur' + str(valeur))	
					if valeur:
						print('poids [0] avant  = '+ str(self.poids[0]))
						self.poids[0] = self.poids[0] + (self.learningRate * (sortieAttendu - sortieObserve) * 1)
						print('poids [0] apres = '+ str(self.poids[0]))
						print('poids [1] avant  = '+ str(self.poids[1]))
						self.poids[1] = self.poids[1] + (self.learningRate * (sortieAttendu - sortieObserve) * paireInput[0])
						print('poids [1] apres = '+ str(self.poids[1]))
						print('poids [2] avant  = '+ str(self.poids[2]))
						self.poids[2] = self.poids[2] + (self.learningRate * (sortieAttendu - sortieObserve) * paireInput[1])
						print('poids [2] apres = '+ str(self.poids[2]))
				j+=1
				valeur = True
			print('sortie')

			j=0


	def __str__(self):
		return 'Perceptron (nombreInput  = {}, nombreEpochs  = {}, learningRate = {})'.format(self.nombreInput, self.nombreEpochs, self.learningRate)


#une classe couche de perceptron
#