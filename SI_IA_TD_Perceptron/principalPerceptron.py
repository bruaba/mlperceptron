# -*- coding: Latin-1 -*-
"""
Created on Wed Nov  6 08:47:02 2019

@author: MARONE Cheikh Ahmet Tidiane Chérif
"""

#Les encodage on a Latin-1 pour windows et utf-8 pour linux

import numpy as np
import perceptron as p


nombreInput = 1
nombreEpochs = 10
learningRate = 2

myPerceptron = p.Perceptron(nombreInput, nombreEpochs, learningRate)
print(myPerceptron) 

listeIIdimensionsInputs =  np.array([[0,0],[0,1],[1,0],[1,1]])

print(listeIIdimensionsInputs)

listeIdimensonOutput = np.array([0,0,0,1])

print(listeIdimensonOutput)

myPerceptron.train(listeIIdimensionsInputs,listeIdimensonOutput)


#listeIIdimensions
#fonction d'activation
            