# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 14:48:27 2019

@author: MARONE Cheikh Ahmet Tidiane Chérif
"""
import numpy as np
import csv
import random
import math
class Perceptron(object):

	"""
	le nombre d’inputs du perceptron, 
	le nombre d’epochs (càd le nombre de boucle d’apprentissage) 
	et un learning rate : le taux d'apprentissage détermine la vitesse à laquelle les poids (dans le cas d'un réseau neuronal) ou les cooefficents (en cas de régression linéaire ou de régression logistique) changent.
	"""
	
	def __init__(self, nombre_input, nombre_hidden, nombre_output, nombre_epochs, learning_rate, fonction_activation): 
		#super(Character, self).__init__()
		self.biais = 1
		self.les_inputs = []
		self.les_hidden = []
		self.la_sortie = 0
		self.liste_valeur_erreur = []
		self.nombre_input = nombre_input
		self.nombre_hidden = nombre_hidden
		self.nombre_output = nombre_output
		self.nombre_epochs = nombre_epochs
		self.learning_rate = learning_rate
		self.fonction_activation = fonction_activation
		rows, cols = (nombre_input+1, nombre_hidden) 
		self.poids_hidden = [[0 for i in range(cols)] for j in range(rows)]
		rows, cols = (nombre_hidden+1, nombre_output) 
		self.poids_sortie = [[0 for i in range(cols)] for j in range(rows)]
		self.entree_output = [0 for i in range(nombre_hidden)]



	"""Les mutateurs"""

	def _get_nombre_input(self):
		return self.nombre_input

	def _get_nombre_epochs(self):
		return self.nombre_epochs

	def _get_learning_rate(self):
		return self.learning_rate

	def _get_fonction_activation(self):
		return self.fonction_activation

	"""Les Accesseurs"""
	
	def _set_nombre_input(self, nombre_input):
		self.nombre_input = nombre_input

	def _set_nombre_epochs(self, nombre_epochs):
		self.nombre_epochs = nombre_epochs
	
	def _set_learning_rate(self, learning_rate):
		self.learning_rate = learning_rate


	"""les fonctionnalitées """

	def le_sigmoid(self, x):
		return 1.0/(1.0 + np.exp(-x))

	def le_ReLU(self, x):
		if x <=0.0 :
			x = 0.0
		return x

	def activation(self, x):
		switcher = {
			"sigmoid": self.le_sigmoid(x),
			"ReLU": self.le_ReLU(x)
		}
		return switcher.get(self._get_fonction_activation(), "aucune fonction choisit")

	def a_recorder(self, file_name, the_header, the_rows):
		with open(file_name,'wt') as f:
			csv_writer = csv.writer(f)
			csv_writer.writerow(the_header)
			csv_writer.writerows(the_rows)

	def save_weights(self, file_name):
		the_rows = self.poids
		with open(file_name,'wt') as f:
			csv_writer = csv.writer(f)
			csv_writer.writerow(the_rows)

	def load_weights(self, file_name):
		with open(file_name,'rt') as f:
			csv_reader = csv.reader(f)
			les_inputs = list(csv_reader)
			i = 0
			for un_poid in les_inputs[0]:
				self.poids[i] = int(un_poid)
				i+=1

	def le_chargement(self, file_name):
		with open(file_name,'rt') as f:
			csv_reader = csv.reader(f)
			les_elements = list(csv_reader)
			le_retour = []
			i = 0
			for un_element in les_elements[0]:
				le_retour.append(int(un_element))
				print('un_element = ' + str(un_element))

		return le_retour
	
	def _load_la_sortie(self, nom_fichier):
		self.la_sortie = self.le_chargement(nom_fichier)

	def _load_les_inputs(self, nom_fichier):
		self.les_inputs = self.le_chargement(nom_fichier)


# on prend 7 rentré qui correspondent aux segement 
# c'est rentrée sont binaire (1 si le LED est allumé)
# on met c rentré dans une cellule
# on a 3 couche (couche d'entré, couche de sortie et couche caché)
# on c rentré avec le biais ce qui fait 8 entré qu'on met 
# dans la couche input puis 
#fonction d'activation en sigmoid venant de deux neurone
#" sortie en decimal"
	def initialisation_poids(self, default_value):
		
		for i in np.arange(self.nombre_input+1):
			for j in np.arange(self.nombre_hidden):
				self.poids_hidden[i][j] = default_value

		for i in np.arange(self.nombre_hidden+1):
			for j in np.arange(self.nombre_output):
				self.poids_sortie[i][j] = default_value

	#fait la somme des wi xi
	def fonction_somme(self, rows, cols, entree, poids, sortie):
		x = 0
		for j in np.arange(cols):
			x = poids[rows][j] #pour le biais qui constitue le 8e input
			print('fonction somme x=' + str(x))
			for i in np.arange(rows):
				x += entree[i] * poids[i][j]
				print('somme des x' + str(x))
				sortie[j] = self.activation(x)



	def train(self):
		self._load_les_inputs("les_inputs.csv")
		self._load_la_sortie("la_sortie.csv")
		self.initialisation_poids(random.random())
		
		j=0
		erreur = []
		sortie_erreur = [0 for i in range(4)] #ampleur de l'erreur au niveau de la sortie
		hidden_erreur = [0 for i in range(9)]
		poids_corrige = 0

		sortie_observee = [0 for i in range(self.nombre_output)]
		sortie_attendu_hidden = [0 for i in range(self.nombre_hidden+1)]
		sortie_attendu = self.la_sortie
		#EntreePoids = self.biais

		self.les_inputs.append(1)
		valeur = False	
		epoch = 0
		somme = 0

		while epoch < self.nombre_epochs:
			self.fonction_somme(self.nombre_input, self.nombre_hidden, self.les_inputs, self.poids_hidden, self.entree_output)
			self.entree_output.append(1)
			self.fonction_somme(self.nombre_hidden, self.nombre_output, self.entree_output, self.poids_sortie, sortie_observee)
			valeur = sortie_observee[0] != sortie_attendu[0]
			
			if valeur:
				
				print('la valeur' + str(valeur))
				sortie_erreur = 0.5 * math.pow((sortie_attendu[0] - sortie_observee[0]),2)
				print('sortie_attendu = {} et sortie_observee = {}'.format(sortie_attendu[0], sortie_observee[0]))
				print('sortie_erreur = ' + str(sortie_erreur))

				for i in np.arange(self.nombre_hidden+1):
					for j in np.arange(self.nombre_output):
						self.poids_sortie[i][j] = self.poids_sortie[i][j] + (self.learning_rate * (sortie_attendu[0] - sortie_observee[0]) * self.entree_output[i])
						print('poids_sortie[{}][{}] = {}'.format(i, j, self.poids_sortie[i][j]))
				
				for i in np.arange(self.nombre_hidden+1):
					for j in np.arange(self.nombre_output):
						somme += self.poids_hidden[i][j]
						print('somme = {}'.format(somme))

				for i in np.arange(self.nombre_hidden+1):
					for j in np.arange(self.nombre_output):
						sortie_attendu_hidden[i] = self.poids_hidden[i][j] / (somme)
						print('sortie_attendu_hidden[{}] = {}'.format(i, sortie_attendu_hidden[i]))

				for i in np.arange(self.nombre_input+1):
					for j in np.arange(self.nombre_hidden):
						self.poids_hidden[i][j] = self.poids_hidden[i][j] + (self.learning_rate * (sortie_attendu_hidden[j] - self.entree_output[j]) * self.les_inputs[i])
						print('poids_hidden[{}][{}] = {}'.format(i, j, self.poids_hidden[i][j]))

			valeur = False
			epoch +=1


	def __str__(self):
		return 'Perceptron (nombre_input  = {}, nombre_epochs  = {}, learning_rate = {})'.format(self.nombre_input, self.nombre_epochs, self.learning_rate)
