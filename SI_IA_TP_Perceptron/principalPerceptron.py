# -*- coding: Latin-1 -*-
"""
Created on Wed Nov  6 08:47:02 2019

@author: MARONE Cheikh Ahmet Tidiane Chérif
"""

#Les encodage on a Latin-1 pour windows et utf-8 pour linux

import perceptron as p


nombre_input = 7
nombre_hidden = 2
nombre_output = 1
nombre_epochs = 1
learning_rate = 0.01
fonction_activation = "sigmoid"


myPerceptron = p.Perceptron(nombre_input, nombre_hidden, nombre_output, nombre_epochs, learning_rate, fonction_activation)
print(myPerceptron)

myPerceptron.train()
